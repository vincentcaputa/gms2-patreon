<?php

// your campaign ID
$campaign_id = ''; 

// the client ID of your Patreon app
$client_id = '';

// the client secret of your Patreon app
$client_secret = '';

// the URI of the page that handles authentication on your server
$redirect_uri = "https://your.site/auth.php";